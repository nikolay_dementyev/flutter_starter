package com.example.starter_project

import android.os.Bundle
import android.widget.Toast
import io.flutter.app.FlutterActivity
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugins.GeneratedPluginRegistrant

class MainActivity: FlutterActivity() {

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    GeneratedPluginRegistrant.registerWith(this)
    var channel = MethodChannel(flutterView, "flutter_app")
    channel.setMethodCallHandler { methodCall, result ->
      val args = methodCall.arguments as List<*>
      val param = args.first() as String

      when(methodCall.method) {
        "showToastMessage" -> showToastMessage(param)
      }
    }
  }
  
  private fun showToastMessage(msg: String) {
    Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
  }
}
