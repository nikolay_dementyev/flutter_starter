**************************** Firebase Integration ************************************************************************************************************

1. Import Package
  - uncomment from 44 to 50 to import firebase packages
2. MultiDex should be enabled
  - Go to android/app/build.gradle. 
  - In defaultConfig, add "multiDexEnabled true"
3. Change project package name to make firebase app for your project
  - Go to android/app/src/main/kotlin/com/example/starter_project/MainActivity.kt
    In line 1, change package name from com.example.starter_project to something you want, e.g. com.jonh.school_app.
  - Change the structure of directory that contains MainActivity.kt
    From android/app/src/main/kotlin/com/example/starter_project to android/app/src/maim/kotlin/com/jonh/school_app
  - In AndroidManifest.xml(debug, main, profile), chage package name from "com.example.starter_project" to something you want. e.g. com.jonh.school_app.
  - In android/app/build.gradle, change applicationId from "com.example.starter_project" to somethig you want, like "com.jonh.school_app"
4. Create FirebaseApp using packange name and download google-services.json file into android/app folder
5. Uncomment "//classpath 'com.google.gms:google-services:4.3.2'" in android/build.gradle
6. Uncomment "//apply plugin: 'com.google.gms.google-services'" in android/app/build.gradle
7. Use the functions and classes in lib/providers/firebase folder.

************************************ The End *******************************************************************************************************************

