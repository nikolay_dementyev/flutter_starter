import 'package:flutter/material.dart';

class BounceButton extends StatefulWidget {
  
  final Widget child;
  final VoidCallback onPressed;
  
  BounceButton({
    Key key,
    @required this.onPressed,
    this.child,
  }) : super(key: key);
  
  @override
  _BounceState createState() => _BounceState();
}

class _BounceState extends State<BounceButton> with SingleTickerProviderStateMixin {
  
  AnimationController _pressController;
  Animation<double> _scaleAnimation;
  
  @override
  void initState() {
    super.initState();
       
    _pressController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 100),
      reverseDuration: Duration(milliseconds: 500)
    );
    
    _scaleAnimation = Tween<double>(begin: 1, end: .75).animate(
      CurvedAnimation(
        parent: _pressController,
        curve: Curves.easeOut,
        reverseCurve: ElasticInCurve()
      )
    );
    _pressController.addListener(() {
      if(_pressController.isDismissed) {
        widget.onPressed();
      }
    });
  }
  
  @override
  void dispose() {
    _pressController.dispose();
    super.dispose();
  }
  
  @override
  Widget build(BuildContext context) {
    return ScaleTransition(
      scale: _scaleAnimation,
      child: InkWell(
        onTap: () {
          _pressController.forward().then((_) {
            _pressController.reverse();
          });
          widget.onPressed();
        },
        child: widget.child,
      ),
    );
  }
}