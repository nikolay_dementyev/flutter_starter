import 'package:flutter/material.dart';

class SearchBar extends StatelessWidget {
  
  final ValueChanged<String> onQuery;
  final TextEditingController controller = TextEditingController();
  
  SearchBar({Key key, this.onQuery}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(color: Colors.orange, width: 7.0)
      ),
      padding: EdgeInsets.symmetric(horizontal: 15),
      child: Row(
        children: <Widget>[
          // InkWell(onTap: () {
          //   BlocProvider.of<NavigatorBloc>(context).add(GoBack());
          // }, child: Icon(Icons.arrow_back, color: Colors.black)),
          // const SizedBox(width: 5),
          Expanded(
            child: TextField(
              controller: controller,
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: 'Search...'
              ),
            ),
          ),
          const SizedBox(width: 5),
          InkWell(onTap: () {
            if(onQuery != null) {
              onQuery(controller.text);
            }
          }, child: Icon(Icons.close, color: Colors.black))
        ]
      ),
    );
  }
}