class Profile {
  
  //// For Mysql Database
  int userId;
  
  //// Firebase UserId 
  String fbUserId;

  String email;
  String userName;
  String password;
  
  Profile({this.fbUserId, this.userId, this.userName, this.password, this.email});
  
  Profile._internalFromJson(Map jsonMap) {
    
  } 
  
  factory Profile.fromJson(Map jsonMap) => Profile._internalFromJson(jsonMap);
}