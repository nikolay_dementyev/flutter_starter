import 'package:equatable/equatable.dart';

abstract class SubmitEvent extends Equatable {
  const SubmitEvent();

  @override
  List<Object> get props => null;
}

class Submit extends SubmitEvent {}

class SubmitSucceeded extends SubmitEvent {}

class SubmitFailed extends SubmitEvent {}