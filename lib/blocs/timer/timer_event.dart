import 'package:equatable/equatable.dart';

abstract class TimerEvent extends Equatable {
  const TimerEvent();

  @override
  List<Object> get props => null;
}

class Tick extends TimerEvent {
  final int ticks;

  Tick(this.ticks);
}

class Start extends TimerEvent {
  final int duration;

  Start(this.duration);
}

class Restart extends TimerEvent {
  final int duration;
  
  Restart({this.duration = -1});
}

class Stop extends TimerEvent {}