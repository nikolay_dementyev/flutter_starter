import 'dart:async';
import 'package:bloc/bloc.dart';
import './bloc.dart';

class LoadBloc extends Bloc<LoadEvent, LoadState> {

  @override
  LoadState get initialState => InitialLoadState();
  
  @override
  Future<void> close() {
    
    return super.close();
  }
  
  @override
  Stream<LoadState> mapEventToState(
    LoadEvent event,
  ) async* {
    if(event is Load) {
      yield Loading();
    } else if(event is LoadSucceeded) {
      yield LoadSuccess();
      await Future.delayed(Duration(seconds: 1));
      yield InitialLoadState();
    } else if(event is LoadFailed) {
      yield LoadFailure();
      await Future.delayed(Duration(seconds: 1));
      yield InitialLoadState();
    }
  }
}