import 'package:shared_preferences/shared_preferences.dart';
import 'package:template/models/index.dart';

class UserRepository {
  
  final SharedPreferences sharedPrefs;
  
  UserRepository(this.sharedPrefs);
  
  bool isLogedIn() {
    return sharedPrefs.getBool('logged_in') ?? false;
  }
  
  Profile loadSession() {
    int userId = sharedPrefs.getInt('user_id');
    String fbUserId = sharedPrefs.getString('fb_user_id');
    String userName = sharedPrefs.getString('user_name');
    String email = sharedPrefs.getString('email');
    Profile profile = Profile(fbUserId: fbUserId, userId: userId, userName: userName, email: email);
    return profile;
  }
  
  void deleteSession() {
    sharedPrefs.setInt('user_id', null);
    sharedPrefs.setString('fb_user_id', null);
    sharedPrefs.setBool('logged_in', false);
    sharedPrefs.setString('user_name', null);
    sharedPrefs.setString('email', null);
  }
  
  void saveSession(Profile profile) {
    sharedPrefs.setInt('user_id', profile.userId);
    sharedPrefs.setBool('logged_in', true);
    sharedPrefs.setString('user_name', profile.userName);
    sharedPrefs.setString('email', profile.email);
    sharedPrefs.setString('fb_user_id', profile.fbUserId);
  }
}