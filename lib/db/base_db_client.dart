import 'dart:io';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:async';
import 'package:path_provider/path_provider.dart';

abstract class BaseDBClient {
  
  static Database _database;
  
  Future<Database> _getDataBase() async {
    if(_database == null) {
      _database = await initDB();
    }
    return _database;
  }
  
  initDB() async {
    Directory documentDirectory = await getApplicationDocumentsDirectory();
    //// Copy Assets/db/app.db to Application Directory 
    // await copyAssetDbToApplicationDirectory('assets/db/app.db');
    String path = join(documentDirectory.path, 'app.db');
    return await openDatabase(path, version: 1, onOpen: (db) async {
      await db.execute('CREATE TABLE IF NOT EXISTS province (_id INTEGER PRIMARY KEY, id INTEGER, name TEXT);');
      await db.execute('CREATE TABLE IF NOT EXISTS city (id INTEGER PRIMARY KEY, name TEXT, link INTEGER);');
    });
  }
  
  Future<List<Map>> runQuery(String query) async {
    Database db = await _getDataBase();
    List<Map> result = await db.rawQuery(query);
    return result;
  } 
}