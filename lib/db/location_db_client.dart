import 'base_db_client.dart';

class LocationDBClient extends BaseDBClient {

  Future<List<String>> getCities(int provinceId) async {
    List<Map> data = await runQuery('SELECT * FROM city where link = $provinceId);');
    return data.map<String>((jsonMap) => jsonMap['name']).toList();
  }
}