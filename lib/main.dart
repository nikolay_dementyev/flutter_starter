import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:template/app.dart';
import 'blocs/index.dart';

void main() async {
  
  WidgetsFlutterBinding.ensureInitialized();
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  final userRepository = UserRepository(sharedPreferences);
  final GlobalKey<NavigatorState> _navigatorKey = GlobalKey();
  
  runApp(
   MultiBlocProvider(
      providers: [
        BlocProvider<NavigatorBloc>(
          create: (context) => NavigatorBloc(navigatorKey: _navigatorKey),
        ),
        BlocProvider<AuthenticationBloc>(
          create: (context) => AuthenticationBloc(userRepository, navigatorBloc: BlocProvider.of<NavigatorBloc>(context)),
        ),
        BlocProvider<LoadBloc>(
          create: (context) => LoadBloc(),
        ),
        BlocProvider<SubmitBloc>(
          create: (context) => SubmitBloc(),
        ),
      ],
      child: MyApp(navigatorKey: _navigatorKey),
    )
  );
}