import 'dart:io';
import 'package:template/models/index.dart';
import 'package:template/singletons/index.dart';
import 'base/app_api_client.dart';

class UserApiClient extends AppApiClient {
  
  static UserApiClient _instance = UserApiClient._internal();
  
  UserApiClient._internal();
  
  factory UserApiClient() {
    return _instance;
  }
  
  Future<Profile> getProfile(int userId) async {
    String url = AppApiClient.BASE_URL + 'Establishments/est_query.php?fliter=Account&keyword=$userId';
    var res = await getRequest(url);
    if(res == null) return null; 
    
    return Profile.fromJson(res['Establishment']);
  }
  
  // Future<Profile> updateProfile(Profile profile) async {
  //   Map<String, File> files = {}; 
    
  //   Map data = await sendMultiPartRequest('Establishments/est_update.php', files, body: profile.toJson());
    
  //   if(data == null) return null;
  //   return Profile.fromJson(data);
  // }
  
  Future<String> logIn(String username, String password) async {
    
    Map<String, dynamic> body = {
      'username': username,
      'password': password,
      'type': 'establishment'
    };
    
    Map data = await postFormData(AppApiClient.BASE_URL + 'Accounts/login.php', body);
    if(data == null) return null;
    
    if(data['loginStatus'] == true) {
      if(data['Account']) {
        AppApiClient.token = data['token'];
        print(data['code']);
        return data['token'];
      } else {
         Global().showToastMessage('Account closed');
         return null;
      }
    } else {
      Global().showToastMessage('الإدخالات غير صحيحة');
      return null;
    }
  }
  
  Future<int> register(String phoneNo, String password) async {
     Map<String, dynamic> body = {
      'username': phoneNo,
      'password': password,
      'type': 'establishment'
    };
    Map data = await postFormData(AppApiClient.BASE_URL + 'Accounts/account_create.php', body);
    if(data == null) return null;
    
    if(data['Created'] == true) {
      print(data['code']);
      AppApiClient.token = data['token'];
      return data['id'];
    } else {
      return null;
    }
  }
  
  Future<bool> recoverPassword(String phoneNo) async {
     Map<String, dynamic> body = {
      'username': phoneNo,
      'type': 'admin'
    };
    Map data = await postFormData(AppApiClient.BASE_URL + 'Accounts/password_recovery.php', body);
    if(data == null) return null;
    if(data['Recovery'] == true) {
      AppApiClient.token = data['token'];
      print(data['code']);
      return true;
    } else {
      Global().showToastMessage('Phone number is not registered');
      return false;
    }
  }
  
  Future<Map> verifyCodeForRecoverPassword(String code) async {
     Map<String, dynamic> body = {
      'code' : code
    };
    Map data = await sendPostRequest('Accounts/2step_auth_recoverypassword.php', jsonMap: body);
    return data;
  }
  
  Future<bool> verifyCode(String code) async {
    Map<String, dynamic> body = {
      'code' : code
    };
    Map data = await sendPostRequest('Accounts/2step_auth_account_create.php', jsonMap: body);
    if(data['authStatus'] == true) {
      return true;
    } else {
      return false;
    }
  }

}