import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart';
import 'package:dio/dio.dart' as dio;

abstract class BaseApiClient {
  
  Future<dynamic> getRequest(String url, {Map<String, String> headers}) async {
    try {
      final response = await get(url, headers: headers);
      final jsonData = json.decode(response.body);
      return jsonData;
    } catch (e) {
      print(e.toString());
      return null;
    }
  }
  
  Future<dynamic> postFormData(String url, Map<String, dynamic> jsonMap) async {
    dio.Dio dioRequest = dio.Dio();
    dio.FormData formData = dio.FormData.fromMap(jsonMap);
    print(jsonMap.toString());
    try {      
      final response = await dioRequest.post(url,data: formData);
      return response.data;
    }  catch (e) {
      return null;
    }
  }
  
  Future<Map> postMultipartRequest(String url, Map<String, File> files, {Map<String, dynamic> params, Function(int progress, int total) progressCallback} ) async {
    
    dio.Dio dioRequest = dio.Dio();
    dio.FormData formData; 
    Map<String, dynamic> body = {};
    files.forEach((key, file) {
      body.putIfAbsent(key, () => dio.MultipartFile.fromFileSync(file.path));
    });
    body.addAll(params);
    formData = dio.FormData.fromMap(body);
    try {
      final response = await dioRequest.post(
        url,
        data: formData,
        onSendProgress: (int sent, int total) {
          if(progressCallback != null) {
            progressCallback(sent, total);
          }
        },
        options: dio.Options(
          responseType: dio.ResponseType.json,
        ),
      );
      print(response.data);
      return response.data as Map;
    } catch (e) {
      return null;
    }
  }
  
  Future<dynamic> postRequest(String url, Map<String, String> headers, Map body) async {
    
    try {
      final response = await post(url, 
          headers: headers,
          body: body
      );
      
      final jsonData = json.decode(response.body); 
      return jsonData;
    } catch (e) {
      print(e.toString());
      return null;
    }
  }
  
  Future<dynamic> deleteRequest(String url, Map<String, String> headers) async {
    try {
      final response = await delete(url, headers: headers);
      final jsonData = json.decode(response.body);
      return jsonData;
    } catch (e) {
      print(e.toString());
      return null;
    }
  } 
  
  Future<dynamic> putRequest(String url, Map<String, String> headers, Map body) async {
    try {
      final response = await put(url,
        headers: headers,
        body: body
      );
      
      final jsonData = json.decode(response.body);
      return jsonData;
    } catch (e) {
      print(e.toString());
      return null;
    }
  }
}