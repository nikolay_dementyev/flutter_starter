
import 'dart:io';
import 'base_api_client.dart';

class AppApiClient extends BaseApiClient {
   
  static String token;
  static int userId;
   
  static const String BASE_URL = 'http://admin.syarahstation.com/api/';
  static const String BASE_IMAGE_URL = 'http://admin.syarahstation.com/';
  
  Future<dynamic> sendPostRequest(String action, {Map<String, dynamic> jsonMap}) async {
    Map<String, dynamic> body = jsonMap ?? {};
    body['token'] = token;
    Map data = await postFormData('$BASE_URL$action', body);
    return data;
  }
  
  Future<Map> sendMultiPartRequest(String action, Map<String, File> files, {Map<String, dynamic> body}) async {
    return await postMultipartRequest('$BASE_URL$action', files, params: body);
  }
}