import 'package:flutter/material.dart';

class SplashPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(width: MediaQuery.of(context).size.width, color: Colors.red, child: Image.asset('assets/images/splash.png', fit: BoxFit.cover)),
    );
  }
}