import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:template/app/login/bloc/login_repository.dart';
import 'package:template/blocs/authentication/bloc.dart';
import 'package:template/blocs/submit/bloc.dart';
import 'package:template/helpers/index.dart';
import 'package:template/singletons/global.dart';
import '../../../models/index.dart';
import './bloc.dart';
import 'package:meta/meta.dart';

class LoginBloc extends Bloc<LoginEvent, dynamic> {
  
  final AuthenticationBloc authenticationBloc;
  final SubmitBloc submitBloc;
  final LoginRepository repository = LoginRepository();
  
  LoginBloc({@required this.authenticationBloc, this.submitBloc}) : assert (authenticationBloc != null);
  
  @override
  dynamic get initialState => null;
  
  @override
  Stream<dynamic> mapEventToState(
    LoginEvent event,
  ) async* {
     if(event is Login) {
       if(event.email.isEmpty) {
         Global().showToastMessage('Please input email');
         return;
       } else if(!isValideEmail(event.email)) {
         Global().showToastMessage('Please input valid email');
         return;
       } else if(event.password.isEmpty) {
         Global().showToastMessage('Please input password');
         return;
       }
       submitBloc?.add(Submit());
       Profile profile = await  repository.logIn(event.email, event.password);
       if(profile == null) {
         submitBloc?.add(SubmitFailed());
       } else {
         submitBloc?.add(SubmitSucceeded());
         Global().showToastMessage('Successfuly logged in');
         await Future.delayed(Duration(milliseconds: 500));
         authenticationBloc.add(LoggedIn(profile: profile));
       }
     } else if(event is GoogleSignIn) {
       await repository.googleSignIn();
     }
  }
}