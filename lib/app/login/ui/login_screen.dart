import 'package:template/app/register/ui/register_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:template/app/login/bloc/bloc.dart';
import 'package:template/blocs/submit/bloc.dart';
import 'package:template/widgets/index.dart';
import '../../common/index.dart';

class LoginScreen extends StatefulWidget {
  
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  
  final TextEditingController _emailController = TextEditingController();
  
  final TextEditingController _passwordController = TextEditingController();
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [
              Color(0xFF8F30A9),
              Color(0xFFBF317A),
              Color(0xFF05ADCF),
            ],
            begin: Alignment.bottomLeft,
            end: Alignment.topRight,
          )
        ),
        child: SafeArea(
          child: Center(
            child: CustomScrollView(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              slivers: <Widget>[
                SliverToBoxAdapter(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Text('Login', style: TextStyle(fontSize: 30, fontFamily: 'Nexabold', fontWeight: FontWeight.bold, color: Colors.white)),
                      const SizedBox(height: 50),
                      Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          InfoInput(controller: _emailController, icon: Icons.person, inputType: TextInputType.emailAddress, hint: 'Email'),
                          const SizedBox(height: 15),
                          InfoInput(controller: _passwordController, icon: Icons.lock, hint: 'Password', obscureText: true),
                          const SizedBox(height: 40),
                          BlocBuilder<SubmitBloc, SubmitState>(
                            builder: (context, state) {
                              return SubmitButton(title: 'Login', onPressed: () {
                                BlocProvider.of<LoginBloc>(context).add(Login(email: _emailController.text, password: _passwordController.text));
                              });
                            },
                          ),
                          const SizedBox(height: 15),
                          GoogleSignButton(onPressed: () {
                            BlocProvider.of<LoginBloc>(context).add(GoogleSignIn());
                          }),
                          const SizedBox(height: 40),
                          InkWell(
                            onTap: () {
                              Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => RegisterPage()));
                            },
                            child: IntrinsicWidth(
                              child: Column(
                                children: <Widget>[
                                  Row(
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      Text('Don\'t you have account ? ', style: TextStyle(fontSize: 16, color: Colors.white)),
                                      Text('Register', style: TextStyle(fontSize: 16, fontFamily: 'Nexabold', color: Colors.white, fontWeight: FontWeight.w600)),
                                    ],
                                  ),
                                  const SizedBox(height: 2),
                                  Container(height: 1, color: Colors.white)
                                ],
                              ),
                            ),
                          ),

                        ],
                      ),
                      const SizedBox(height: 40),
                    ]
                  ),
                )
              ],
            ),
          )
        ),
      ),
    );
  }
}