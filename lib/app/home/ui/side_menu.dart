import 'package:flutter/material.dart';
import 'package:template/app/home/bloc/bloc.dart';
import 'package:template/blocs/index.dart';
import 'package:template/dialogs/index.dart';

TextStyle menuItemStyle = TextStyle(color: Colors.grey, fontSize: 18);

class _DrawerHeaderWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    
    return DrawerHeader(
      child: Column(
        children: <Widget>[
          Image.asset(
            'assets/images/logo.png',
            width: 100,
            height: 72,
          ),
          Text(
            'Starter App',
            style: Theme.of(context)
                .textTheme
                .subhead
                .copyWith(color: Colors.white),
          ),
        ],
      ),
      decoration: const BoxDecoration(color: Color(0xBB343b4a)),
    );
  }
}

class DrawerMenuWidget extends StatefulWidget {
  const DrawerMenuWidget({
    Key key,
    @required this.icon,
    @required this.title,
    @required this.onTap,
    @required this.index,
  }) : super(key: key);
  
  final IconData icon;
  final String title;
  final Function onTap;
  final int index;
  
  @override
  _DrawerMenuWidgetState createState() => _DrawerMenuWidgetState();
}

class _DrawerMenuWidgetState extends State<DrawerMenuWidget> {
  @override
  Widget build(BuildContext context) {
    return ListTile(
        leading: Icon(widget.icon,
            color: Colors.black
            ),
        title: Text(
          widget.title,
          style: Theme.of(context).textTheme.body1,
        ),
        onTap: () {
          widget.onTap(widget.title, widget.index);
        });
  }
}

typedef MenuItemClickCallback(String title, int index);

class NavMenuWidget extends StatelessWidget {
  
  final MenuItemClickCallback onMenuItemSelected;
  
  const NavMenuWidget({Key key, @required this.onMenuItemSelected}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
   return Column(
     children: <Widget>[
       Expanded(
         child: ListView(padding: EdgeInsets.zero, children: <Widget>[
              _DrawerHeaderWidget(),
              ListTile(
                title: Text('Home'),
              ),
              DrawerMenuWidget(
                  icon: Icons.store,
                  title: 'Home',
                  index: 1,
                  onTap: (String title, int index) {
                    Navigator.pop(context);
                    onMenuItemSelected(title, index);
                  }),
              DrawerMenuWidget(
                  icon: Icons.category,
                  title: 'Category',
                  index: 2,
                  onTap: (String title, int index) {
                    Navigator.pop(context);
                    onMenuItemSelected(title, index);
                  }),
              const Divider(
                height: 1,
              ),
              ListTile(
                title: Text('Profile'),
              ), 
              ListTile(
                leading: Icon(
                  Icons.power_settings_new,
                  color: Colors.black,
                ),
                title: Text('Log out',
                  style: Theme.of(context).textTheme.body1,
                ),
                onTap: () {
                  Navigator.pop(context);
                  showDialog<dynamic>(
                      context: context,
                      builder: (BuildContext context) {
                        return ConfirmDialogView(
                            description: 'Are you sure you want to log out?',
                            leftButtonText: 'Cancel',
                            rightButtonText: 'Ok',
                            onAgreeTap: () {
                              BlocProvider.of<AuthenticationBloc>(context).add(LoggedOut());
                            });
                      });
                },
              ),
              const Divider(
                height: 1,
              ),
              ListTile(
                title: Text('App'),
              ),
              DrawerMenuWidget(
                  icon: Icons.g_translate,
                  title: 'Language',
                  index: 3,
                  onTap: (String title, int index) {
                    Navigator.pop(context);
                    onMenuItemSelected(title, index);
                  }),
              DrawerMenuWidget(
                  icon: Icons.contacts,
                  title: 'Contact Us',
                  index: 4,
                  onTap: (String title, int index) {
                    Navigator.pop(context);
                    onMenuItemSelected(title, index);
                  }),
            ]),
       ),
      const SizedBox(height: 15,)
     ],
   );
  }
}