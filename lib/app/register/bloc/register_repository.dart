import 'package:template/singletons/global.dart';
import '../../../models/index.dart';

class RegisterRepository {
  Future<Profile> register({String email, String userName, String password, String passConf}) async {
    if(userName.isEmpty) {
      Global().showToastMessage('Please input username');
      return null;
    }
    if(email.isEmpty) {
      Global().showToastMessage('Please input email');
      return null;
    }
    if(password.isEmpty) {
      Global().showToastMessage('Please input password');
      return null;
    }
    if(password != passConf) {
      Global().showToastMessage('Password does not match');
      return null;
    }
    await Future.delayed(Duration(seconds: 1));
    return Profile(userName: userName, email: email);
  }
}