import 'package:template/app/login/ui/login_page.dart';
import 'package:template/app/register/bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:template/app/login/bloc/bloc.dart';
import 'package:template/blocs/submit/bloc.dart';
import 'package:template/widgets/index.dart';
import '../../common/index.dart';

class RegisterScreen extends StatefulWidget {
  
  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  final TextEditingController _emailController = TextEditingController();

  final TextEditingController _usernameController = TextEditingController();

  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _passconfController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [
              Color(0xFF8F30A9),
              Color(0xFFBF317A),
              Color(0xFF05ADCF),
            ],
            begin: Alignment.bottomLeft,
            end: Alignment.topRight,
          ),
        ),
        child: SafeArea(
          child: Center(
            child: CustomScrollView(
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              slivers: <Widget>[
                 SliverToBoxAdapter(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Text('Register', style: TextStyle(fontSize: 30, fontFamily: 'Nexabold', fontWeight: FontWeight.bold, color: Colors.white)),
                      const SizedBox(height: 50),
                      Center(
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            InfoInput(controller: _usernameController, icon: Icons.person, hint: 'Username'),
                            const SizedBox(height: 15),
                            InfoInput(controller: _emailController, icon: Icons.person, inputType: TextInputType.emailAddress, hint: 'Email'),
                            const SizedBox(height: 15),
                            InfoInput(controller: _passwordController, icon: Icons.lock, hint: 'Password', obscureText: true),
                            const SizedBox(height: 15),
                            InfoInput(controller: _passconfController, icon: Icons.lock_outline, hint: 'Password', obscureText: true),
                            const SizedBox(height: 40),
                            BlocBuilder<SubmitBloc, SubmitState>(
                              builder: (context, state) {
                                return SubmitButton(title: 'Register', onPressed: () {
                                  BlocProvider.of<RegisterBloc>(context).add(Register(email: _emailController.text, password: _passwordController.text, userName: _usernameController.text, passConf: _passconfController.text));
                                });
                              },
                            ),
                            const SizedBox(height: 40),
                            InkWell(
                              onTap: () {
                                Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => LoginPage()));
                              },
                              child: IntrinsicWidth(
                                child: Column(
                                  children: <Widget>[
                                    Row(
                                      mainAxisSize: MainAxisSize.min,
                                      children: <Widget>[
                                        Text('You have account already ? ', style: TextStyle(fontSize: 16, color: Colors.white)),
                                        Text('Login', style: TextStyle(fontSize: 16, fontFamily: 'Nexabold', color: Colors.white, fontWeight: FontWeight.w600)),
                                      ],
                                    ),
                                    const SizedBox(height: 2),
                                    Container(height: 1, color: Colors.white)
                                  ],
                                ),
                              ),
                            ),

                          ],
                        ),
                      ),
                      const SizedBox(height: 40),
                    ]
                  ),
                )
              ],
            ),
          )
        ),
      ),
    );
  }
}