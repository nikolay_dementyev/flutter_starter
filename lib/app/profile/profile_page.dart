import 'dart:io';

import 'package:template/helpers/file_helper.dart';
import 'package:template/widgets/index.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mdi/mdi.dart';

class ProfilePage extends StatefulWidget {

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  
  File _avatar;
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text('Profile'),
      ),
      body: Column(
        children: <Widget>[
          const SizedBox(height: 20),
          CircleAvatar(
            backgroundImage: _avatar == null ? AssetImage('assets/images/avatar.png') : FileImage(_avatar),
            radius: 80,
          ),
          const SizedBox(height: 20),
          Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              InkWell(onTap: () {
                pickImage(ImageSource.camera).then((image) {
                  setState(() {
                    _avatar = image;
                  });
                });
              }, child: Icon(Mdi.camera, size: 40,)),
              const SizedBox(width: 10),
              InkWell(onTap: () {
                pickImage(ImageSource.gallery).then((image) {
                  setState(() {
                    _avatar = image;
                  });
                });
              }, child: Icon(Mdi.image, size: 40,)),
            ],
          ),
          const SizedBox(height: 20),
          SubmitButton(
            color: Colors.green,
            title: 'Edit',
          ),
        ],
      ),
    );
  }
}