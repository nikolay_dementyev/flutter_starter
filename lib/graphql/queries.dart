import 'package:meta/meta.dart';

class Queries {
  static String userLogin({@required String username, @required String password}) {
    return '''
      query{
        userLogin(username:"$username", password:"$password"){
            status
            message
            resultId
          }
      }
    ''';
  }
}