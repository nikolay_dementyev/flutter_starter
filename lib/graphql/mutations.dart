import 'package:meta/meta.dart';

class Mutations {
  
  static String verifyOTP({@required String otp, @required int tempRegisterId}) {
    return '''
    mutation{
      verifyOTP(otp: "$otp", TempRegisterId: $tempRegisterId) {
        status,
        message,
        resultId
      }
    }
    ''';
  }
  
}