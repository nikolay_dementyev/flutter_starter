import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:template/singletons/index.dart';

const String GRAPHQL_ENDPOINT = 'https://w54qow1tgc.execute-api.us-east-2.amazonaws.com/dev/graphql';
const String SUBSCRIPTION_ENDPOINT = 'ws://w54qow1tgc.execute-api.us-east-2.amazonaws.com/dev/graphql';

class GraphQLProvider {
  
  static final GraphQLProvider _instance = GraphQLProvider._internal();
  
  factory GraphQLProvider() {
    return _instance;
  }
  
  GraphQLProvider._internal() {
     Link link = HttpLink(uri: GRAPHQL_ENDPOINT);
    if (SUBSCRIPTION_ENDPOINT != null) {
      final WebSocketLink websocketLink = WebSocketLink(
        url: SUBSCRIPTION_ENDPOINT,
        config: SocketClientConfig(      
          autoReconnect: true,
          delayBetweenReconnectionAttempts: Duration(seconds: 1),
          inactivityTimeout: Duration(seconds: 60),
        ),
      );  
      
      link = link.concat(websocketLink);
      
      _client =  GraphQLClient(
        cache: InMemoryCache(),
        link: link,
      );
    }
  }
  
  GraphQLClient _client;
  
  Future<Map> runQuery(String query, {String key}) async {
    print(query);
    try {
      QueryOptions queryOptions = QueryOptions(document: query);
      var res = await _client.query(queryOptions);
      if(res.data == null) {
        Global().showToastMessage('Internal Server Error\n${query.toString()}');
        return null;
      }
      return res.data as Map;
    } catch (e) {
      return null;
    }
  }
}